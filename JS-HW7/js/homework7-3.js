const arrToList = (list, container) => {
    const ulList = document.createElement('ul');
    const listArr = list.map(elem => {
        ulList.insertAdjacentHTML("afterbegin", `<li>${elem}</li>`)


    });
    container.prepend(ulList);
}

arrToList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], document.body);