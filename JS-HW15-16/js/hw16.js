// Написать функцию для подсчета n-го обобщенного числа Фибоначчи. Аргументами на вход будут три
// числа - F0, F1, n, где F0, F1 - первые два числа последовательности (могут быть любыми целыми числами),
// n - порядковый номер числа Фибоначчи, которое надо найти. Последовательнось будет строиться по следующему
// правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
//     Считать с помощью модального окна браузера число, которое введет пользователь (n).
//     С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
//     Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).

// ITERATION

// function fibo (n){
//     let numFirst = 1;
//     let numSecond = 1;
//
//     for (let i = 3; i <= n; i++) {
//         let numNext = numFirst + numSecond;
//         numFirst = numSecond;
//         numSecond = numNext;
//     }
//     return numSecond;
// }

//RECURSION

function fibo (n){
//debugger
    return n <= 0 ? n : fibo(n - 1) + fibo( n - 2);


}


let countFibo = +prompt("What number of Fibo are you looking for?");
alert("We think you searched this one --> " + fibo(countFibo));