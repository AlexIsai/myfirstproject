const buttons = document.querySelectorAll('.btn');
const areaButtons = document.querySelector('.btn-wrapper');

window.addEventListener('keydown', (e)=>{
    let activeKey = e.key.toLowerCase();
    buttons.forEach((e)=>{
        if (e.classList.contains('active-color')){
            e.classList.remove('active-color');
        }

        if (e.classList.contains(activeKey)){
            e.classList.add('active-color')
        }
    })
})