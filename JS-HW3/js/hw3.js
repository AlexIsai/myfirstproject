// Задание
// Реализовать функцию, которая будет производить математические операции с введеными пользователем числами.
//     Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     Считать с помощью модального окна браузера два числа.
//     Считать с помощью модального окна браузера математическую операцию, которую нужно совершить.
//     Сюда может быть введено +, -, *, /.
// Создать функцию, в которую передать два значения и операцию.
//     Вывести в консоль результат выполнения функции.
//
//
//     Необязательное задание продвинутой сложности:
//
//     После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа,
//     - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация)

let numFirst = prompt("Input the first integer, plz");
let numSecond = prompt("Input the second integer, plz");

while ((!Number.isInteger(+numFirst)) || (!Number.isInteger(+numSecond))){
    numFirst = prompt("Don't make me angry! Input the first integer! - ", numFirst);
    numSecond = prompt("Don't make me angry! Input the second integer! - ", numSecond);
}

let operation = prompt("What would you like to do? Choose, plz, '+', '-', '*' or '/'");

while((operation !== '+') && (operation !== '-') && (operation !=='*') && (operation !== '/')){
    operation = prompt("Don't make me angry! What would you like to do? Choose, plz, '+', '-', '*' or '/'");
}

function calcValue (firstNumber, secondNumber, operation) {
    switch (operation) {
        case "+":
            return firstNumber + secondNumber;
        case "-":
            return firstNumber - secondNumber;
        case "*":
            return firstNumber * secondNumber;
        case "/":
            return firstNumber / secondNumber;
    }
}

alert(numFirst + " " + operation + " " + numSecond + " = " + calcValue(+numFirst, +numSecond, operation));