ДОМАШНЯЯ РАБОТА №1

Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
Почему объявлять переменную через var считается плохим тоном?

var - устаревшая версия универсальной глобальной переменной. Главное
ее отличие от let/const - область видимости. Переменную вар видно вне циклов, функций,
условных операторов. Переменная вар существует и до объявления (т.е. занимает место)

let - универсальная переменная, значение которой можно менять + область видимости переменной
ограничена {}

const - тот же let, но переменная не предполагает изменение значений

Собственно глобальность переменной var и делает ее использование "плохим тоном". Особенно, 
критично это в проектах, где работает несколько людей. 1 пообъявлял переменные вар-ом, а другие
использовали те же имена для своих переменных и случится конфликт значений переменных.

ДОМАШНЯЯ РАБОТА №2

Описать своими словами в несколько строчек, зачем в программировании нужны циклы.

Главным образом для избежания повторения кода для разных элементов (массивов, последовательностей и тп),
для сортировки и/или обработки данных.

Для валидации, которая должна осуществляться, пока условия не станут истинными.

Для подсчета количества одинаковых операций.

ДОМАШНЯЯ РАБОТА №3

Описать своими словами для чего вообще нужны функции в программировании.
Описать своими словами, зачем в функцию передавать аргумент.

Функции нужны для того, чтобы программный код (условия, вычисления, циклы и прочее) могли быть
выполнены в разных местах программы путем вызова функции, а не копирования кода функции.

Функции необходимо передать аргументы для того, чтобы программный код внутри кода мог их обработать, 
согласно механизму функции. Это также дает возможность одной функции обрабатывать разные значения аргументов и
получать разный результат.

