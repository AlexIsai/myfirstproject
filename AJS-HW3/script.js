"use strict";
class Employee {
    constructor(elements) {
        this._name = elements.name;
        this._age = elements.age;
        this._salary = elements.salary;
    }
    get name() {
        return this._name;
    };
    set name (value) {
        return this._name = value;
    }

    get age() {
        return this._age;
    };
    set age (value) {
        return this._age = value;
    }

    get salary() {
        return this._salary;
    };
    set salary (value) {
        return this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(elements) {
        super(elements);
        this.lang = elements.lang;
    }
    get salary() {
        return this._salary * 3;
    };
    set salary (value) {
        return this._salary = value;
    }
}

const jobs = new Programmer({
    name: 'steve',
    age: 33,
    salary: 100,
    lang: 'english))'
})

const durov = new Programmer({
    name: 'pavel',
    age: 33,
    salary: 100,
    lang: 'russian))'
})

const nakamoto = new Programmer({
    name: 'satoshi',
    age: 33,
    salary: 100,
    lang: 'japan))'
})

console.log(jobs);
console.log(durov);
console.log(nakamoto);