// В файле index.html лежит разметка для двух полей ввода пароля.
//     По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь,
//     иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
//     Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения
//
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.

const activeForm = document.querySelector('.password-form');
const activeButton = document.querySelector('button');
const inputPassword = document.querySelector('.input-pass');
const confirmPassword = document.querySelector('.confirm-pass');
const errorSpan = document.createElement('span');
errorSpan.textContent = 'Нужно ввести одинаковые значения';
errorSpan.style.color = 'red';



activeForm.addEventListener('click', (event)=>{
    let cursorTarget = event.target;
    if(event.target.classList.contains('btn')) return
    if (cursorTarget.classList.contains('fa-eye-slash')){
        cursorTarget.classList.remove('fa-eye-slash')
        cursorTarget.classList.add('fa-eye')
        cursorTarget.closest('.input-wrapper').querySelector('input').type = 'text';
    } else {
        cursorTarget.classList.add('fa-eye-slash')
        cursorTarget.closest('.input-wrapper').querySelector('input').type = 'password';
    }
})

inputPassword.addEventListener('focus', ()=> errorSpan.remove());
confirmPassword.addEventListener('focus', ()=> errorSpan.remove());

activeButton.addEventListener('click', ()=>{
    errorSpan.remove();
    if (inputPassword.value === confirmPassword.value && inputPassword.value !== ""){
        alert('You are welcome!');
    } else activeButton.before(errorSpan)
})