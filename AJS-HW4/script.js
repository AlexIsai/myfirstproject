/*
Задание
Получить список фильмов серии Звездные войны, и вывести на экран список персонажей для каждого из них.

Технические требования:

Отправить AJAX запрос по адресу https://swapi.dev/api/films/ и получить список всех фильмов серии Звездные войны

Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей можно получить из свойства characters.
Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl).
Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.
 */

fetch('https://swapi.dev/api/films/', {
    method: 'GET',
}).then(res =>{
    return res.json();
}).then(smth => {
    return smth.results;
}).then(info => {
    info.forEach(seriya => {
        let film = document.createElement('div')
        document.body.append(film);
        film.insertAdjacentHTML('afterbegin', `<h2>${seriya.title}</h2><p>${seriya.episode_id}</p><p>${seriya.opening_crawl}</p>`)
        seriya.characters.forEach(el =>{
            let str = el.slice(4, el.length);
            str = "https"+str;

            fetch(`${str}`, {
                method: 'GET',
            }).then(yaya =>{
                return yaya.json();
            }).then (uh => {
                film.insertAdjacentHTML('beforeend', `<p>${uh.name}</p>`);
            })
         })
    })
})