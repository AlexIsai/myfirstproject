// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//     При вызове функция должна спросить у вызывающего имя и фамилию.
//     Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//     Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
//     соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести
// в консоль результат выполнения функции.
//
//
//     Необязательное задание продвинутой сложности:
//
//     Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры
// setFirstName() и setLastName(), которые позволят изменить данные свойства.


function createNewUser(fName, sName) {
    const user = {
        firstName : fName,
        secondName : sName,

        getLogin : function (fName, sName){
           return this.firstName[0].toLowerCase() + this.secondName.toLowerCase();
        }

    }; return user;
}

const newUser = createNewUser(prompt("Enter your first name"), prompt("Enter your second name"));
console.log(newUser);
console.log(newUser.getLogin());

