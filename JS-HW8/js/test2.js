// При загрузке страницы показать пользователю поле ввода (input) с надписью Price.
//     Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При
// потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span, в
// котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним
// должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение,
//     введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода
// красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
//
//     В папке img лежат примеры реализации поля ввода и создающегося span.
const textBlock = document.querySelector('.text-block');
const labelPrice = document.createElement('label');
labelPrice.textContent = 'PRICE: ';
const formPrice = document.createElement('input');
formPrice.placeholder = 'Input the price here';
const buttonX = document.querySelector('.button-x');
buttonX.textContent = "X";
textBlock.style.display = 'none';
buttonX.style.cssText = `
    
    width : 24px;
    height : 24px;
    border-radius : 50%;
    color : grey;
    border : 2px solid grey;
    `
const errorPrice = document.createElement('div');
errorPrice.textContent = 'Please enter correct price.'
errorPrice.style.color = 'red';
const priceText = document.querySelector('.price-text');
document.body.prepend(formPrice);
document.body.prepend(labelPrice);

formPrice.addEventListener('focus', () => {
    if (formPrice.classList.contains('invalid') || formPrice.value === "") {
        formPrice.classList.remove('invalid');
        formPrice.classList.add('focused');
    }
});
formPrice.addEventListener('blur', () => {
    formPrice.classList.remove('focused');
    priceText.textContent = `Текущая цена: ${formPrice.value} USD`;
    if (Number(formPrice.value) <= 0 || isNaN(formPrice.value)) {
        formPrice.classList.add('invalid');
        document.body.append(errorPrice);
    } else {
        formPrice.classList.add('focused');
        formPrice.style.color = "green";
        formPrice.style.width = 'bold';
        textBlock.style.display = 'block';
        document.body.prepend(textBlock);

        errorPrice.remove();

    }
})
buttonX.addEventListener('click', () => {
    formPrice.value = '';
    formPrice.classList.remove('invalid');
    formPrice.classList.remove('focused');
    errorPrice.remove();
    formPrice.style.color = '';
    formPrice.style.width = '';
    textBlock.style.display = 'none';
})

