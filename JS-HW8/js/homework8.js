// При загрузке страницы показать пользователю поле ввода (input) с надписью Price.
//     Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При
// потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span, в
// котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним
// должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение,
//     введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода
// красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
//
//     В папке img лежат примеры реализации поля ввода и создающегося span.
// debugger
const price = document.createElement('label');
price.textContent = 'PRICE';
const error = document.createElement('span');
error.textContent = 'Please enter correct price';

const textPrice = document.createElement('span');
textPrice.textContent = `Текущая цена: }`

const priceValue = document.createElement('input');
priceValue.classList.add('check-value');
priceValue.name = 'price here';
priceValue.placeholder = 'Input price here';


// input.addEventListener('focus', () => {
//     input.classList.add('focused');
// });



priceValue.onblur = () => {
    if (Number(priceValue.value) <= 0 && priceValue.value === "") {
        priceValue.classList.add('focused');
        error.innerHTML = 'Please enter correct price.'
    }
};


priceValue.onfocus = function() {
    if (this.classList.contains('focused')) {

        this.classList.remove('focused');
        error.innerHTML = "";
    }
};




document.body.prepend(price);
document.body.append(priceValue);
console.dir(priceValue.value);
