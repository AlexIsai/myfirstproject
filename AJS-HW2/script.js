// Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован
// с помощью Javascript).
// На странице должен находиться div с id="root", куда и нужно будет положить этот список (похожая
// задача была дана в модуле basic).
// Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться
// все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна
// высветиться ошибка с указанием - какого свойства нету в обьекте.
//     Те элементы массива, которые являются некорректными по условиям предыдущего пункта,
//     не должны появиться на странице.
const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const place = document.getElementById('root');





const booksToList = (books, place) => {
    const isEnoughProperties = item => {
        if (!Object.keys(item).includes('author')) {
            throw new Error('author is empty')
        }
        if (!Object.keys(item).includes('name')) {
            throw new Error('name is empty')
        }
        if (!Object.keys(item).includes('price')) {
            throw new Error('price is empty')
        }
    }

    books.forEach((item) => {
        try {
        let {
            author,
            name,
            price,
        } = item;

        if (Object.keys(item).includes('author') && Object.keys(item).includes('name') && Object.keys(item).includes('price')) {
            place.insertAdjacentHTML('beforeend', `<ul><li>${author}</li><ul><li>name - ${name}</li><li>price - ${price}</li></ul></ul>`)
        } else {
            isEnoughProperties(item);
        }
    }
    catch (e){
        console.error(e);
    }
    });

}
booksToList(books, place);