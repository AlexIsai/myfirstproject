function dragDrop() {
    const cardList = document.querySelector('.card-list');
    const visits = document.querySelectorAll('.visits')


    for (const drag of visits) {
        drag.draggable = true;

    }

    cardList.addEventListener(`dragstart`, (evt) => {
        let cursorTarget = evt.target
        cursorTarget.classList.add(`selected`);
    });

    cardList.addEventListener(`dragend`, (evt) => {
        let cursorTarget = evt.target
        cursorTarget.classList.remove(`selected`);
    });

    cardList.addEventListener(`dragover`, (evt) => {
        evt.preventDefault()
        let currentElement = evt.target
        let activeElement = document.querySelector('.selected')

        if (!(activeElement !== currentElement && currentElement.classList.contains('visits'))) {
            return;
        }
        let nextElement = (currentElement === activeElement.previousElementSibling) ?
            currentElement.previousElementSibling :
            currentElement;
        cardList.insertBefore(activeElement, nextElement)

    });
}

export default {
    dragDrop
}