import change from "../module/Change-form.js";
import modal from "../module/classModal.js";

let some = change.ChangeFormContent


function rediReload () {
    let listCard = document.querySelectorAll('.visits')
    listCard.forEach(item => {
        item.remove()
    })
    showCards()
}

function cardDelete (cardId) {
    console.log('Card id from delete', cardId);
    fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${"b72f0991-4578-4644-9fb3-a034dde491d8"}`
        },
    }).then(res => {
        console.log('res del', res);
    })
}

function showCards() {
    setTimeout(emptyList, 400)
    fetch(`https://ajax.test-danit.com/api/v2/cards/`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${"b72f0991-4578-4644-9fb3-a034dde491d8"}`
        },
    }).then(result => {
            return result.json();
        }
    ).then(arr => {
            arr.forEach(item => {
                // cardDelete(item.id)
                if (item.doctor === "Therapist"){
                new TherapistCard(item).therapyCardRender()
            }
                if (item.doctor === "Dentist"){
                    new DentistCard(item).dentistCardRender()
                } if (item.doctor === "Cardiologist"){
                    new CardioCard(item).cardioCardRender()
                }
            })
        }
    )

}

showCards();
class DoctorsCard {
    constructor(item) {
        this.nameValue = item.name;
        this.titleValue = item.title;
        this.descriptionValue = item.description;
        this.doctorValue = item.doctor;
        this.targetValue = item.target;
        this.cardId = item.id;
        this.emergency = item.emergency;
        localStorage.setItem('some', "Status")
        this.elements = {
            selectStatus: localStorage.getItem('some'),
            cardBoard: document.querySelector('.board'),
            cardList: document.querySelector('.card-list'),
            cardContainer: document.createElement('li'),
            cardGeneralInfo: document.createElement('div'),
            cardTitle: document.createElement('h4'),
            cardName: document.createElement('p'),
            cardDoctor: document.createElement('p'),
            cardBtnShow: document.createElement('button'),
            cardHiddenDiv: document.createElement('div'),
            cardHiddenBtn: document.createElement('div'),
            cardTarget: document.createElement('p'),
            cardDescription: document.createElement('p'),
            cardStatus: document.createElement('h4'),
            cardBtnChange: document.createElement('button'),
            cardSelectChange: document.createElement('select'),
            cardBtnOk: document.createElement('button'),
            cardCheckDiv: document.createElement('div'),
            cardCheckSelect: document.createElement('select'),
            cardCheckBtn: document.createElement("button")
        }


    }

    getContent() {
        let {
            cardContainer,
            cardGeneralInfo,
            cardTitle,
            cardName,
            cardDoctor,
            cardBtnShow,
            cardHiddenDiv,
            cardTarget,
            cardDescription,
            cardStatus,
            cardBtnChange,
            cardSelectChange,
            cardBtnOk,
            cardCheckBtn,
            cardCheckSelect,
            cardCheckDiv,
            selectStatus
        } = this.elements;
        cardTitle.textContent = `${this.titleValue}`;
        cardName.textContent = `${this.nameValue}` || undefined;
        cardDoctor.textContent = `${this.doctorValue}`;
        cardBtnShow.textContent = `Показать больше`;
        cardTarget.textContent = `Цель визита: ${this.targetValue}`;
        cardDescription.textContent = `Описание: ${this.descriptionValue}` || undefined;
        cardStatus.textContent = `Срочность: ${this.emergency}` || undefined;
        cardBtnChange.textContent = 'EDIT';
        cardSelectChange.innerHTML = `<option>Change</option><option>Delete</option>`;
        cardBtnOk.textContent = 'OK';
        // selectStatus = localStorage.getItem(`${this.cardId}`)
        // cardCheckSelect.innerHTML = `<option class="select-default">${selectStatus}</option><option value="Open">Open</option><option value="Done">Done</option>`;
        cardCheckBtn.textContent = 'OK';
        this.getSelectValue()
    }

    getStyles() {
        let {
            cardContainer,
            cardGeneralInfo,
            cardTitle,
            cardName,
            cardDoctor,
            cardBtnShow,
            cardHiddenDiv,
            cardTarget,
            cardDescription,
            cardStatus,
            cardHiddenBtn,
            cardBtnChange,
            cardSelectChange,
            cardBtnOk,
            cardCheckDiv,
            cardCheckSelect,
        } = this.elements;
        cardCheckSelect.classList.add('card-status')
        cardContainer.classList.add('visits')
        cardGeneralInfo.classList.add('info')
        cardTitle.classList.add('item-elem', "card-title")
        cardName.classList.add('item-elem')
        cardDoctor.classList.add('item-elem')
        cardBtnShow.classList.add('item-elem')
        cardBtnShow.classList.add('button-action')
        cardHiddenDiv.classList.add('hide-info')
        cardHiddenBtn.classList.add('hide-btn')
        cardHiddenBtn.classList.add('hide-btn-container')
        cardTarget.classList.add('card-text')
        cardStatus.classList.add('card-text', 'card-priority')
        cardCheckDiv.classList.add('check-status')
        cardSelectChange.classList.add('card-select')
        cardBtnChange.classList.add('btn')
        cardBtnChange.classList.add('button-action')
        cardBtnOk.classList.add(`${this.cardId}`)
        cardContainer.classList.add(`${this.cardId}`)
        cardDescription.classList.add('card-text', 'card-description')
    }
    getSelectValue(){
        setTimeout(()=> {
            let {cardCheckSelect, selectStatus} = this.elements
             selectStatus = localStorage.getItem(`${this.cardId}`)
            if (selectStatus === null) {
                selectStatus = 'Status'
            }
             cardCheckSelect.innerHTML = `<option class="select-default">${selectStatus}</option><option value="Open">Open</option><option value="Done">Done</option>`;
        },300)
    }

    handler() {
        let {cardBtnShow, cardHiddenDiv, cardBtnChange, cardHiddenBtn, cardCheckBtn, cardCheckSelect, cardSelectChange, cardBtnOk, cardContainer, cardBoard, cardList} = this.elements;
        cardBtnShow.addEventListener('click', (e) => {
            if (!cardHiddenDiv.classList.contains('change-hide-info')) {
                cardBtnShow.textContent = 'Скрыть'
                cardHiddenDiv.classList.add('change-hide-info')
                cardHiddenDiv.classList.remove('hide-info')
            } else {
                cardBtnShow.textContent = 'Показать больше'
                cardHiddenDiv.classList.add('hide-info')
                cardHiddenDiv.classList.remove('change-hide-info')
            }
        })
        cardBtnChange.addEventListener('click', (e) => {
            cardHiddenBtn.classList.toggle('hide-btn')
            cardBtnChange.remove()

        })

        cardCheckBtn.addEventListener('click', (e) => {
            let checkStatus = cardCheckSelect.value;
            localStorage.setItem('some', checkStatus)
            localStorage.setItem(`${this.cardId}`, checkStatus)

        })
        cardBtnOk.addEventListener('click', (e) => {
            let cursorTarget = e.target;
            if (cardSelectChange.value === "Delete"){
            cardDelete(cursorTarget.className);
                if (cardSelectChange.value === "Delete"){
                    console.log('before', cardList.children)
                    console.log(cardBoard);
                    console.log(cursorTarget.className);
                    cardDelete(cursorTarget.className);
                    cardContainer.remove()
                    emptyList()
                }

            } if (cardSelectChange.value === "Change") {
                let test = new change.ChangeFormContent(cursorTarget.className)
                let changeModal = new modal.ClassModal
                changeModal.render()
                test.render(cursorTarget.className)
            }
        })

    }

    render() {
        let {
            cardBoard,
            cardList,
            cardContainer,
            cardGeneralInfo,
            cardTitle,
            cardName,
            cardDoctor,
            cardBtnShow,
            cardHiddenDiv,
            cardTarget,
            cardDescription,
            cardStatus,
            cardHiddenBtn,
            cardBtnChange,
            cardSelectChange,
            cardBtnOk,
            cardCheckBtn,
            cardCheckSelect,
            cardCheckDiv
        } = this.elements;
        this.getContent()
        this.getStyles()
        this.handler()
        this.getSelectValue()
        cardCheckDiv.append(cardCheckSelect, cardCheckBtn)
        cardHiddenBtn.append(cardSelectChange, cardBtnOk)
        cardHiddenDiv.append(cardTarget, cardDescription, cardStatus, cardHiddenBtn, cardBtnChange)
        cardGeneralInfo.append(cardTitle, cardName, cardDoctor, cardCheckDiv, cardBtnShow,);
        cardContainer.append(cardGeneralInfo, cardHiddenDiv);
        cardList.append(cardContainer)
        cardBoard.append(cardList)
    }
}

class CardioCard extends DoctorsCard {
    constructor(item) {
        super(item);
        this.pressure = item.pressure;
        this.indexBody = item.indexBody;
        this.seek = item.seek;
        this.age = item.age;
        this.elements = {
            ...this.elements,
            cardPressure: document.createElement('p'),
            cardIndexBody: document.createElement('p'),
            cardSeek: document.createElement('p'),
            cardAge: document.createElement('p'),
        }
    }

    cardioCardgetContent() {
        let {
            cardPressure,
            cardIndexBody,
            cardSeek,
            cardAge
        } = this.elements;
        // console.log(this.imb);
        cardPressure.textContent = `Обычное давление: ${this.pressure}` || undefined;
        cardIndexBody.textContent = `Индекс массы тела: ${this.indexBody}` || undefined;
        cardSeek.textContent = `Заболевания ССС: ${this.seek}` || undefined;
        cardAge.textContent = `Возраст: ${this.age}` || undefined;
    }

    cardioCardgetStyles() {
        let {
            cardContainer,
            cardPressure,
            cardIndexBody,
            cardSeek,
            cardAge
        } = this.elements;
        cardContainer.classList.add('cardio')
        cardPressure.classList.add('card-text')
        cardIndexBody.classList.add('card-text')
        cardSeek.classList.add('card-text')
        cardAge.classList.add('card-text')
    }

    cardioCardRender() {
        this.render()
        let {
            cardHiddenDiv,
            cardPressure,
            cardIndexBody,
            cardSeek,
            cardAge,
            cardBtnChange,
            cardHiddenBtn,
        } = this.elements;
        this.cardioCardgetContent()
        this.cardioCardgetStyles()
        cardHiddenDiv.append(cardPressure, cardSeek, cardAge, cardIndexBody, cardBtnChange, cardHiddenBtn)
    }

}

class DentistCard extends DoctorsCard {
    constructor(item) {
        super(item);
        this.lastDate = item.lastDate;
        this.elements = {
            ...this.elements,
            cardLastDate: document.createElement('p')
        }
    }

    dentistGetContent() {
        let {
            cardLastDate
        } = this.elements
        cardLastDate.textContent = `Последний визит: ${this.lastDate}` || undefined
    }

    dentistGetstyles() {
        let {cardLastDate, cardContainer} = this.elements;
        cardLastDate.classList.add('card-text');
        cardContainer.classList.add('dentist')
    }

    dentistCardRender() {
        this.render()
        let {
            cardHiddenDiv,
            cardBtnChange,
            cardHiddenBtn,
            cardLastDate
        } = this.elements;
        this.dentistGetContent();
        this.dentistGetstyles();
        cardHiddenDiv.append(cardLastDate, cardBtnChange, cardHiddenBtn)
    }
}

class TherapistCard extends DoctorsCard{
    constructor(item) {
        super(item);
        this.userAge = item.age
        this.elements = {
            ...this.elements,
            cardUserAge: document.createElement('p')
        }
    }
    therapyGetContent() {
        let {cardUserAge} = this.elements;
        cardUserAge.textContent = `Возраст: ${this.userAge}` || undefined;
    }
    therapyGetStyles() {
        let {cardContainer, cardUserAge} = this.elements;
        cardUserAge.classList.add('card-text');
        cardContainer.classList.add('therapy')
    }
    therapyCardRender() {
        this.render()
        let {
            cardHiddenDiv,
            cardBtnChange,
            cardHiddenBtn,
            cardUserAge
        } = this.elements;
        this.therapyGetContent();
        this.therapyGetStyles();
        cardHiddenDiv.append(cardUserAge, cardBtnChange, cardHiddenBtn)
    }

}




// console.log(new CardioCard());

    // document.addEventListener('DOMContentLoaded', function () {
    //     const SELECTS = document.querySelectorAll('select.linked__data, input.linked__data');
    //     console.log(SELECTS);
    //     setTimeout(()=>{
    //         const LIST = document.querySelectorAll('.visits');
    //         const listArr = [...LIST]
    //         console.log(LIST);
    //         const result = listArr.filter(item =>{
    //             if (item == ){
    //             }
    //         })
    //         console.log(result);
    //     },400)
    //
    // })

function emptyList() {
    const cardBoard = document.querySelector('.board')
    const cardList = document.querySelector('.card-list')
    const empty = document.querySelector('.empty')
    console.log('cardlist', cardList);
    console.log('child', cardList.children);
    if (cardList.children.length === 0){
        empty.style.display = 'block'
        cardBoard.prepend(empty)
    } if (cardList.children.length >= 1) {
        empty.style.display = 'none'
    }
}





export default {
    emptyList,
    rediReload,
    showCards,
    DoctorsCard,
    CardioCard,
    DentistCard,
    TherapistCard,
    cardDelete
}