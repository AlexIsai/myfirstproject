const $menuList = $('.list-menu');
const $btn = $('<button class="go-up">GO UP</button>');
const $burgerBtn = $('.burger');
const $hiddingBlock = $('.for-artem');


$(window).scroll(function (event) {
    let windowHeight = $(window).scrollTop();
    console.log($(window).height())
    console.log(windowHeight)
    if (windowHeight >= $(window).height()) {
        $(document.body).prepend($btn);
        $btn.removeClass('hidden');
    } if (windowHeight <= $(window).height()) {
        $btn.addClass('hidden');
    }
});

$menuList.click((e) => {
    let cursorTarget = e.target;
    let hrefId = $(cursorTarget).attr('href')
    let topHeight = $(hrefId).offset().top
    $('html, body').animate({scrollTop : topHeight}, 1000);
})

$btn.click(()=>{
    $('html, body').animate({scrollTop : 0}, 1000);
})

$burgerBtn.click(()=>{
    $burgerBtn.toggleClass('burger_toggle');
    $hiddingBlock.slideToggle('hidden')
})