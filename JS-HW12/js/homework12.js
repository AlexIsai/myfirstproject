const pictures = document.querySelectorAll('.image-to-show');
const stopButton = document.querySelector('.stop');
const startButton = document.querySelector('.go-on');
console.log(pictures[0]);
let counter = 0;

let showPictures = setInterval(imgShow, 3000);

function imgShow() {
    pictures[counter].classList.remove('show')
    counter++;
    if (counter === pictures.length) {
        counter = 0;
    }
    pictures[counter].classList.add('show')
}

stopButton.addEventListener('click', () => {
    clearInterval(showPictures);
})
startButton.addEventListener('click', () => {
    setInterval(imgShow, 3000);
})
