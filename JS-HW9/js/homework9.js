const clickContainer = document.querySelector('.tabs');
const activeElement = document.querySelectorAll('.tabs-title')
const activeContent = document.querySelectorAll("li");
clickContainer.addEventListener('click', (e)=>{
    let activeTarget = e.target;
    let search = activeTarget.dataset.name;

    activeElement.forEach((item) =>{
        if (item.classList.contains('active')){
            item.classList.remove('active');
        }
    });
    activeTarget.classList.add('active');
    activeContent.forEach((elm)=> {
        if (elm.classList.contains('show')) {
            elm.classList.remove('show')
        }
        if (elm.classList.contains(search)) {
            elm.classList.add('show');
        }
    })
})