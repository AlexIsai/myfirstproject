// Задание 6
// Дан обьект employee. Добавьте в него свойства age и salary, не изменяя изначальный объект
// (должен быть создан новый объект, который будет включать все необходимые свойства).
// Выведите новосозданный объект в консоль.

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const {
    name,
    surname
} = employee

const boxer = {
    name,
    surname,
    age: 50,
    salary: 1000,
}

console.log(boxer);



// const newEmployee ={
//     name: name,
//     surname:surname,
//     age:50,
//     salary:1000
// }
// console.log(newEmployee);
